'use strict';

/**
*
* NAMING CONVENTIONS:
* -------------------
* Singleton-literals and prototype objects:  PascalCase
*
* Functions and public variables:            camelCase
*
* Global variables and constants:            UPPERCASE
*
* Private variables:                         _underscorePrefix
*
*/

var gulp            = require('gulp'),
    babelify        = require('babelify'),
    browserSync     = require('browser-sync'),
    browserify      = require('browserify'),
    del             = require('del'),
    source          = require('vinyl-source-stream'),
    buffer          = require('vinyl-buffer'),
    transform       = require('vinyl-transform'),
    stringify       = require('stringify'),
    changed         = require('gulp-changed');

var watchify        = require('watchify'),
    debowerify      = require('debowerify');

// $ is a namespace for all gulp-pluginname npm modules.
var $ = require('gulp-load-plugins')();

var _ = {
        app: 'app',
        dist: 'dist',
        reload: browserSync.reload,
        isProduction: (process.env.NODE_ENV === 'production' || !!$.util.env.production),
        bundles: [{
            src: 'app/scripts/main.js',
            bundle: 'bundle.js'
        }]
    },
    bundler = {};

// ✓ styles
gulp.task('styles', function () {
    return gulp.src(_.app + '/styles/style.scss')
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.sass({
            outputStyle: _.isProduction ? 'compressed' : 'expanded'
        }))
        .on('error', $.sass.logError)
        .pipe($.autoprefixer('last 2 version', 'ie >= 9'))
        .pipe(_.isProduction ? $.sourcemaps.write('./maps') : $.sourcemaps.write())
        .pipe(gulp.dest(_.dist + '/styles'))
        .pipe(browserSync.stream({match: '**/*.css'}));
});
// /end styles

// ✓ browserify
gulp.task('browserify', function () {
    function task(file) {
        var options     = watchify.args,
            cache       = {},
            filename    = file.src,
            notify      = function (filename) {
                $.util.log($.util.colors.green('√') + ' ' + filename);
            };

        var bundler = function (options) {
            // return previous bundle
            if (cache[filename]) {
                return cache[filename].bundle();
            }

            var b = browserify(filename, {
                paths: ['./node_modules', './app']
            });

            // transforms
            b.transform(babelify, { presets: ['es2015'] });
            b.transform(stringify(['.html']));

            // events
            b.on('bundle', notify.bind(null, 'Bundled: ' + filename));

            b = watchify(b, options)
                .on('update', bundle);

            // cache for use during watch
            cache[filename] = b;

            return b.bundle();
        };

        var bundle = function (check) {
            return bundler(options)
                // log errors if they happen
                .on('error', function (err) {
                    $.util.log('Browserify Error: ' + err.message);
                    browserSync.notify(err.message, 3000);
                })
                .pipe(source(file.bundle))
                // optional, remove if you dont want sourcemaps
                .pipe(buffer())
                .pipe($.sourcemaps.init({ loadMaps: true })) // loads map from browserify file
                .pipe($.uglify())
                .pipe($.sourcemaps.write('./')) // writes .map file
                .pipe(gulp.dest(_.dist + '/scripts'))
                .pipe(browserSync.stream({ once: true }));
        };

        bundle(false);
    }

    // Watchify all bundles
    for (var i = 0; i < _.bundles.length; i++) {
        task(_.bundles[i]);
    }
});
// /end browserify

// ✓ jshint
gulp.task('jshint', function () {
    return gulp.src([
            _.app + '/scripts/**/*.js',
            '!' + _.app + '/scripts/vendor/**/*.js'
        ])
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.jshint.reporter('fail'));
});
// /end jshint

// ✓ html
gulp.task('html', ['styles', 'modernizr'], function () {
    var lazypipe = require('lazypipe');
    var cssChannel = lazypipe()
        .pipe($.csso)
        .pipe($.replace, 'fonts');

    var assets = $.useref.assets({ searchPath: '{.tmp, app}' });

    return gulp.src([
            _.app + '/**/*.html',
            '!' + _.app + '/templates/**/*.html',
            '!' + _.app + '/partials/**/*.html',
            '!' + _.app + '/scripts/vendor/**/*.html',
        ])
        .pipe(assets)
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', cssChannel()))
        .pipe(assets.restore())
        .pipe($.useref())
        .pipe(gulp.dest(_.dist));
});
// /end html

// ✓ images
gulp.task('images', function () {
    return gulp.src(_.app + '/assets/images/**/*')
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(_.dist + '/assets/images'));
});
// /end images

// ✓ watch
gulp.task('watch', ['clean', 'styles', 'browserify', 'nodemon'], function () {
    var port = process.env.PORT || 4000;

    browserSync({
        proxy: 'http://localhost:' + port,
        // We're using Google Chrome as standard @ Vertic
        browser: 'google chrome',
        files: [_.dist + '/scripts/bundle.js'],
        reloadDelay: 500
    });

    gulp.watch(_.app + '/styles/**/*.scss', ['styles']);
});
// /end watch

// ✓ nodemon
gulp.task('nodemon', ['hbs', 'images', 'fonts'], function (cb) {
    var called = false;

    return $.nodemon({
            script: 'server/server.js',
            ext: 'js html',
            watch: ['server/**/*.js', '**/*.html'],
            ignore: ['node_modules/**/*']
        })
        .on('start', function () {
            if (!called) {
                cb();
            }
            called = true;
        })
        .on('restart', browserSync.reload);
});
// /end nodemon

// ✓ compile handlebars
gulp.task('hbs', ['html'], function () {
    var options = {
        batch : ['./app/partials']
    };

    var assets = $.useref.assets({ searchPath: '{.tmp, app}' });

    return gulp.src('./app/templates/**/*.html')
        .pipe($.compileHandlebars({}, options))
        .pipe($.useref())
        .pipe(gulp.dest(_.dist + '/built-templates'));
});
// /end handlebars

// ✓ fonts
gulp.task('fonts', function () {
    return gulp.src([_.app + '/assets/fonts/**/*', _.app + '/scripts/vendor/components-font-awesome/fonts/**/*'])
        .pipe(gulp.dest(_.dist + '/assets/fonts'));
});
// /end fonts

// ✓ modernizr
gulp.task('modernizr', function () {
    return gulp.src(_.app + '/scripts/**/*.js')
        .pipe($.modernizr({
            tests: ['touchevents'],
            options: [
                'setClasses',
                'addTest',
                'html5printshiv',
                'testProp',
                'fnBind',
                'mq'
            ]
        }))
        .pipe($.uglify())
        .pipe(gulp.dest(_.dist + '/scripts/'));
});
// /end modernizr

// ✓ clean
gulp.task('clean', function() {
    return del.sync(['.tmp', 'dist']);
});
// /end clean

// ✓ build
gulp.task('build', ['clean', 'hbs', 'browserify', 'images', 'fonts'], function () {
    return gulp.src('dist/**/*')
        .pipe($.size({ title: 'build', gzip: true }))
        .once('end', function () {
            process.exit();
        });
});
// /end build

gulp.task('default', ['watch']);
