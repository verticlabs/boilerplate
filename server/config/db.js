/**
* DB configuration
*
* @author rkalms <rkalms@gmail.com>
*/

'use strict';

module.exports = function (environment) {
	var config = {};

	switch (environment) {
		default:
			config.connection = 'mongodb://test:test@localhost/some-db';

			break;
	}

	return config;
};