'use strict';
 
// Simple express server
var express = require('express'),
	fs = require('fs'),
	hbs = require('hbs');

var app = express();
var router = express.Router();
var routes = require('./routes');

var partialsDir = './app/partials';
var filenames = fs.readdirSync(partialsDir);

/**
* Config
*/
app.use(express.static('app'), express.static('dist'));

/**
* Handlebars setup
*/
app.set('view engine', 'html');
app.engine('html', hbs.__express);

app.set('views', 'app/templates');

// Register partials
filenames.forEach(function (filename) {
	var matches = /^([^.]+).html$/.exec(filename);

	if (!matches) {
		return;
	}

	var name = matches[1];
	var template = fs.readFileSync(partialsDir + '/' + filename, 'utf8');

	hbs.registerPartial(name, template);
});

/**
* Routes
*/
routes(app);

app.listen(4000);
