/**
* Routes
* 	Routing wrapper.
*
* @author rkalms <rkalms@gmail.com>
*/

'use strict';

var fs = require('fs');
var dir = process.env.appvirtdir || '/',

	// Routes
	web = require('./web');

/**
* readJsonFileSync()
*
* @param {String} Filepath
* @param {String} Encoding (optional)
*/
function readJsonFileSync(filepath, encoding){
	if (typeof (encoding) == 'undefined'){
		encoding = 'utf8';
	}

	var file = fs.readFileSync(filepath, encoding);

	return JSON.parse(file);
}

module.exports = function (app) {
	// Enable different routes
	web(app);

	app.all(dir + '*', function (req, res) {
		res.sendStatus(404);
	});
};

