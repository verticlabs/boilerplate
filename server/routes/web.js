/**
* Web routes
*
* @author rkalms <rkalms@gmail.com>
*/

'use strict';

var fs = require('fs');
//var _ = require('lodash');
var dir = process.env.appvirtdir || '/';

module.exports = function(app){

	// Add your templates here
	var pageTemplates = [],
		globalData = {};

	// Serve index.html
	app.get('/', function (req, res) {
		var data = {};

		res.render('index', data);
	});

	// List of current templates
	app.get('/list', function (req, res) {
		var data = {
			templates: pageTemplates
		};

		res.render('list', data);
	});

	// Serve pageTemplates
	pageTemplates.forEach(function(pageTemplate) {
		app.get(dir + pageTemplate, function (req, res) {
			res.render(pageTemplate, globalData);
		});
	});
};
