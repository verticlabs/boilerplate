/**
* MongoDB commands
*/

// Create user
db.createUser(
    {
      user: "test",
      pwd: "test",
      roles: [
         { role: "readWrite", db: "auction" }
      ]
    }
)