/*
 * Vertic JS - Site functional wrapper
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

'use strict';

var $ = require('jquery'),
    _ = require('underscore'),
    Core = require('vertic-core-library');

$(function () {
    console.log('app init');
});
