# Project title

A short description of the project.

* Lead developer:
* Extra dependencies:
* Link to tech spec:

## Additional information:
* Explanation about patterns or frameworks used that are not part of the standard boilerplate.
* Relevant knowledge (did you hack an external library again?)

## Code features

* BEM like SCSS structure.
* Development for fast frontend prototyping.
* Javascript linting through [`ESLint`](http://eslint.org/).
* SCSS linting through [`scss_lint`](https://github.com/brigade/scss-lint/).
* Image optimization with
  [`imagemin`](https://github.com/sindresorhus/gulp-imagemin)
* Live browser reloading with [`Browsersync`](https://www.browsersync.io/)
* New ES6 (ES2015) style javascript syntax through [`Babel`](https://babeljs.io/)
* Includes:
    * [`normalize.css`](https://necolas.github.com/normalize.css/)
      for CSS normalizations and common bug fixes.
    * [`jQuery`](https://jquery.com/) packaged in the local bundle.
    * A custom build of  [`Modernizr`](http://modernizr.com/) for feature
      detection. Will dynamically traverse the code to customize the build
      the needed features.
    * [`Backbone`](http://backbonejs.org/) for structured MVC based javascript.
    * [`Marionette`](http://marionettejs.com/) for structured views in Backbone.

## Dependencies

* [`NodeJS >=4.2.x`](https://nodejs.org)
* [`Python ~2.7.x`](https://www.python.org/) Used by `node-gyp` for certain
  packages.
* `gulp ~3.9.x` - Install globally with `npm install -g gulp`
* `bower ~1.7.x` - Install globally with `npm install -g bower`

### Optional

* [`Ruby >=2.2.x`](https://www.ruby-lang.org) To enable `SCSS`-linting.

## Installation guide

1. Run `npm install -m` (Note: `bower install` is automatically run by `npm install`).
2. Run the application with `npm start` or build the application with `npm run build`.

### Optional

1. Install [`scss_lint`](https://github.com/brigade/scss-lint/) - `gem install scss_lint`
